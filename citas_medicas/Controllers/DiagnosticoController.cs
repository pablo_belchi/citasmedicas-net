﻿using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using citas_medicas.Services.IServices;
using citas_medicas.DTO;
using citas_medicas.Models;

namespace citas_medicas.Controllers
{
    [Route("Diagnostico")]
    [ApiController]
    public class DiagnosticoController : Controller
    {
        private readonly IDiagnosticoService DiagnosticoService;
        private readonly IMapper mapper;

        public DiagnosticoController(IDiagnosticoService DService, IMapper mapper)
        {
            DiagnosticoService = DService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Mostrar todos los diagnosticos
        /// </summary>
        /// <returns>Retorna la lista de diagnosticos</returns>
        [HttpGet]
        public List<DiagnosticoDTO> Index()
        {
            var _Diagnostico = DiagnosticoService.GetAll();
            var _DiagnosticoDTO = mapper.Map<List<DiagnosticoDTO>>(_Diagnostico);
            return _DiagnosticoDTO;
        }

        /// <summary>
        /// Mostrar un diagnostico por Id
        /// </summary>
        /// <returns>Retorna un diagnostico</returns>
        /// /// <param name="Id"></param>
        [HttpGet("{Id}")]
        public DiagnosticoDTO GetById(long Id)
        {
            DiagnosticoDTO _DiagnosticoDTO;

            if (DiagnosticoExists(Id))
            {
                var _Diagnostico = DiagnosticoService.GetById(Id);
                _DiagnosticoDTO = mapper.Map<DiagnosticoDTO>(_Diagnostico);
            }
            else
            {
                _DiagnosticoDTO = null;
            }

            return _DiagnosticoDTO;

        }
        ///// <summary>
        ///// Inserta un diagnostico
        ///// </summary>
        ///// <param name="CitaDTO"></param>
        //[HttpPost]
        //public async Task Insert(NewDiagnosticoDTO DiagnosticoDTO)
        //{
        //    Diagnostico _diagnostico = mapper.Map<Diagnostico>(DiagnosticoDTO);
        //    await CitaService.Insert(_diagnostico);
        //}

        //POST: Citas
        /// <summary>
        /// Eliminar diagnostico
        /// </summary>
        /// <param name="Id"></param>
        [HttpDelete]
            public async Task Delete(long Id)
            {
                if (DiagnosticoExists(Id))
                {
                    await DiagnosticoService.Delete(Id);
                }
            }

        /// <summary>
        /// Editar diagnostico
        /// </summary>
        [HttpPut] 
        public async Task<DiagnosticoDTO> Edit(DiagnosticoDTO Obj)
        {
            var _diagnostico = mapper.Map<Diagnostico>(Obj);
            if (DiagnosticoExists(_diagnostico.Id))
            {
                _diagnostico = DiagnosticoService.Update(_diagnostico).Result;
                return mapper.Map<DiagnosticoDTO>(_diagnostico);
            }

            return null;
        }

        //Validación de elementos
        private bool DiagnosticoExists(long id)
            {
                var _Diagnostico = DiagnosticoService.GetAll();
                return _Diagnostico.Any(e => e.Id == id);
            }
       
    }
}
