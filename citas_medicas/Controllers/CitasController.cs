﻿using Microsoft.AspNetCore.Mvc;
using citas_medicas.Models;
using AutoMapper;
using citas_medicas.DTO;
using citas_medicas.Services.IServices;

namespace citas_medicas.Controllers
{
    [Route("Citas")]
    [ApiController]
    public class CitasController : Controller
    {
        private readonly ICitaService CitaService;
        private readonly IDiagnosticoService DiagnosticoService;
        private readonly IMapper mapper;

        
        public CitasController(ICitaService CService, IMapper mapper)
        {
            CitaService=CService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Mostrar todas las citas
        /// </summary>
        /// <returns>Retorna la lista de citas</returns>
        [HttpGet]
        public List<CitaDTO> GetAll()
        {
            var _cita = CitaService.GetAll();
            var _citaDTO = mapper.Map<List<CitaDTO>>(_cita);
            return _citaDTO;
        }

        /// <summary>
        /// Mostrar una cita por Id
        /// </summary>
        /// <returns>Retorna una cita</returns>
        /// /// <param name="Id"></param>
        [HttpGet("{Id}")]
        public CitaDTO GetId(long Id)
        {
            CitaDTO _citaDTO;

            if (CitaExists(Id))
            {
                var _cita = CitaService.GetById(Id);
                _citaDTO = mapper.Map<CitaDTO>(_cita);
            }
            else
            {
                _citaDTO = null;
            }

            return _citaDTO;

        }

        /// <summary>
        /// Agregar una cita
        /// </summary>
        /// <param name="CitaDTO"></param>
        [HttpPost]
        public async Task Insert(NewCitaDTO citaDTO)
        {
            Cita _cita = mapper.Map<Cita>(citaDTO);
            await CitaService.Insert(_cita);
        }

        /// <summary>
        /// Eliminar cita
        /// </summary>
        /// <param name="Id"></param>
        [HttpDelete]
        public async Task Delete(long Id)
        {
            if (CitaExists(Id))
            {
                await CitaService.Delete(Id);
            }
        }

        /// <summary>
        /// Editar cita
        /// </summary>
        [HttpPut]
        public async Task<CitaDTO> Edit(CitaDTO Obj)
        {
            var _cita = mapper.Map<Cita>(Obj);
            if (CitaExists(_cita.Id))
            {
                _cita = CitaService.Update(_cita).Result;
                return mapper.Map<CitaDTO>(_cita);
            }

            return null;
        }

        //Validación de elementos
        [HttpGet]
        private bool CitaExists(long id)
        {
            var _cita = CitaService.GetAll();
            return _cita.Any(e => e.Id == id);
        }

    }
}
