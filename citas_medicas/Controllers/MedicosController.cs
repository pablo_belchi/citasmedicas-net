﻿using Microsoft.AspNetCore.Mvc;
using citas_medicas.Models;
using AutoMapper;
using citas_medicas.Services.IServices;
using citas_medicas.DTO;

namespace citas_medicas.Controllers
{
    
    [Route("Medicos")]
    [ApiController]
    public class MedicosController : Controller
    {
        
        private readonly IMedicoService MedicoService;
        private readonly IMapper mapper;

        public MedicosController(IMedicoService MedicoService, IMapper mapper)
        {
            this.MedicoService = MedicoService;
            this.mapper = mapper;
        }

        /// <summary>
        /// Mostrar todos los medicos
        /// </summary>
        /// <returns>Retorna la lista de medicos</returns>
        [HttpGet]
        public List<MedicoDTO> Getall()
        {
            var _Medico = MedicoService.GetAll();
            var _MedicoDTO = mapper.Map<List<MedicoDTO>>(_Medico);
            return _MedicoDTO;
        }

        /// <summary>
        /// Mostrar un medico por Id junto con las citas
        /// </summary>
        /// <returns>Retorna un medico</returns>
        /// <param name="Id"></param>
        [HttpGet("{Id}")]
        public Object GetMedicoId(long Id)
        {
            

            if (MedicoExists(Id))
            {
                var _Medico = MedicoService.GetMedicoById(Id);
                
 
                return _Medico;
            }
            else
            {
                return null;
            }
        }

        ///// <summary>
        ///// Mostrar un medico por Id
        ///// </summary>
        ///// <returns>Retorna un medico</returns>
        ///// <param name="Id"></param>
        //[HttpGet("{Id}")]
        //public MedicoDTO GetId(long Id)
        //{
        //    MedicoDTO _MedicoDTO;

        //        if (MedicoExists(Id))
        //        {
        //            var _Medico = MedicoService.GetById(Id);
        //            _MedicoDTO = mapper.Map<MedicoDTO>(_Medico);
        //            return _MedicoDTO;
        //        }
        //        else
        //        {
        //            _MedicoDTO = null;
               
        //            return null;
        //        }

            
       //}

        /// <summary>
        /// Agregar un medico
        /// </summary>
        /// <param name="MedicoDTO"></param>
        [HttpPost]
        public async Task Create(NewMedicoDTO MedicoDTO)
        {
            var _Medico = mapper.Map<Medico>(MedicoDTO);

            await MedicoService.Insert(_Medico);
        }

        /// <summary>
        /// Eliminar un medico
        /// </summary>
        /// <param name="Id"></param>
        [HttpDelete]
        public async Task Delete(long Id)
        {
            if (MedicoExists(Id))
            {
                await MedicoService.Delete(Id);
            }

        }

        /// <summary>
        /// Editar medico
        /// </summary>
        [HttpPut]
        public async Task<MedicoDTO> Edit(MedicoDTO Obj)
        {
            var _medico = mapper.Map<Medico>(Obj);
            if (MedicoExists(_medico.Id))
            {
                _medico = MedicoService.Update(_medico).Result;
                return mapper.Map<MedicoDTO>(_medico);
            }

            return null;
        }


        private bool MedicoExists(long id)
        {
            var _Usuario = MedicoService.GetAll();
            return _Usuario.Any(e => e.Id == id);
        }


    }

    //Segmentar en objetos el Object devuelto por GetMedicoId
    //public class Almacen<T>
    //{
    //    private T[] objetos;
    //    private int i = 0;

    //    public Almacen(int n_entidades = 3)
    //    {
    //        objetos = new T[n_entidades];
    //    }

    //    public void Agregar(T Obj)
    //    {
    //        objetos[i]=Obj;
    //        i++;
    //    }
    //}
}
