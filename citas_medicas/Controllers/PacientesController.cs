﻿using Microsoft.AspNetCore.Mvc;
using citas_medicas.Models;
using AutoMapper;
using citas_medicas.Services.IServices;
using citas_medicas.DTO;

namespace citas_medicas.Controllers
{
    
    [Route("Pacientes")]
    [ApiController]
    public class PacientesController : Controller
    {
        private readonly IPacienteService PacienteService;
        private readonly IMapper mapper;


        public PacientesController(IPacienteService PService, IMapper mapper)
        {
            PacienteService = PService;
            this.mapper = mapper;
        }


        /// <summary>
        /// Mostrar todos los pacientes
        /// </summary>
        /// <returns>Retorna la lista de pacientes</returns>
        [HttpGet]
        public List<PacienteDTO> Getall()
        {
            var _Paciente = PacienteService.GetAll();
            var _PacienteDTO = mapper.Map<List<PacienteDTO>>(_Paciente);
            return _PacienteDTO;
        }

        /// <summary>
        /// Mostrar un paciente por Id
        /// </summary>
        /// <returns>Retorna un paciente</returns>
        /// /// <param name="Id"></param>
        [HttpGet("{Id}")]
        public PacienteDTO GetId(long Id)
        {
            PacienteDTO _PacienteDTO;

            if (PacienteExists(Id))
            {
                var _Paciente = PacienteService.GetById(Id);
                _PacienteDTO = mapper.Map<PacienteDTO>(_Paciente);
            }
            else
            {
                _PacienteDTO = null;
            }

            return _PacienteDTO;
        }

        /// <summary>
        /// Agregar un Paciente
        /// </summary>
        /// <param name="pacienteDTO"></param>
        [HttpPost]
        public async Task Create(NewPacienteDTO pacienteDTO)
        {
            var _Paciente = mapper.Map<Paciente>(pacienteDTO);

            await PacienteService.Insert(_Paciente);
        }

        /// <summary>
        /// Eliminar un Paciente
        /// </summary>
        /// <param name="Id"></param>
        [HttpDelete]
        public async Task Delete(long Id)
        {
            if (PacienteExists(Id))
            {
                await PacienteService.Delete(Id);
            }
            
        }

        /// <summary>
        /// Editar Paciente
        /// </summary>
        [HttpPut]
        public async Task<PacienteDTO> Edit(PacienteDTO Obj)
        {
            var _paciente = mapper.Map<Paciente>(Obj);
            if (PacienteExists(_paciente.Id))
            {
                _paciente = PacienteService.Update(_paciente).Result;
                return mapper.Map<PacienteDTO>(_paciente);
            }

            return null;
        }

        //Validación de elementos
        private bool PacienteExists(long id)
        {
            var _Paciente = PacienteService.GetAll();
            return _Paciente.Any(e => e.Id == id);
        }
    }
}
