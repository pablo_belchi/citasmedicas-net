﻿using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using citas_medicas.DTO;
using citas_medicas.Services.IServices;
using citas_medicas.Services;
using citas_medicas.Models;

namespace citas_medicas.Controllers
{
   
    [Route("Usuarios")]
    [ApiController]
    public class UsuariosController : Controller
    {
        
       private readonly IUsuarioService usuarioService;
       private readonly IMapper mapper;

       public UsuariosController(IUsuarioService usuarioService, IMapper mapper)
       {
           this.usuarioService = usuarioService;
           this.mapper = mapper;
       }

        /// <summary>
        /// Mostrar todos los usuarios
        /// </summary>
        /// <returns>Retorna la lista de usuarios</returns>
        [HttpGet]
        public List<UsuarioDTO> Getall()
        {
            var _Usuario = usuarioService.GetAll();
            var _UsuarioDTO = mapper.Map<List<UsuarioDTO>>(_Usuario);
            return _UsuarioDTO;
        }

        /// <summary>
        /// Mostrar un usuario por Id
        /// </summary>
        /// <returns>Retorna un usuario</returns>
        /// <param name="Id"></param>
        [HttpGet("{Id}")]
        public UsuarioDTO GetId(long Id)
        {
            UsuarioDTO _UsuarioDTO;

            if (UsuarioExists(Id))
            {
                var _Usuario = usuarioService.GetById(Id);
                _UsuarioDTO = mapper.Map<UsuarioDTO>(_Usuario);
            }
            else
            {
                _UsuarioDTO = null;
            }

            return _UsuarioDTO;
        }

        ///// <summary>
        ///// Agregar un usuario
        ///// </summary>
        ///// <param name="UsuarioDTO"></param>
        //[HttpPost]
        //public async Task Create(NewUsuarioDTO UsuarioDTO)
        //{
        //    var _Usuario = mapper.Map<Usuario>(UsuarioDTO);

        //    await usuarioService.Insert(_Usuario);
        //}

        /// <summary>
        /// Eliminar un usuario
        /// </summary>
        /// <param name="Id"></param>
        [HttpDelete]
        public async Task Delete(long Id)
        {
            if (UsuarioExists(Id))
            {
                await usuarioService.Delete(Id);
            }

        }

        /// <summary>
        /// Editar usuario
        /// </summary>
        [HttpPut]
        public async Task<UsuarioDTO> Edit(UsuarioDTO Obj)
        {
            var _usuario = mapper.Map<Usuario>(Obj);
            if (UsuarioExists(_usuario.Id))
            {
                _usuario = usuarioService.Update(_usuario).Result;
                return mapper.Map<UsuarioDTO>(_usuario);
            }

            return null;
        }

        private bool UsuarioExists(long id)
       {
           var _Usuario = usuarioService.GetAll();
           return _Usuario.Any(e => e.Id == id);
       }
   
    }
   
}
