﻿using AutoMapper;
using citas_medicas.DTO;
using citas_medicas.Models;

namespace citas_medicas.Mapping
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {//TODO:

            //Usuario
            CreateMap<Usuario, UsuarioDTO>();
            CreateMap<UsuarioDTO, Usuario>();
            //CreateMap<Usuario, NewUsuarioDTO>();
            //CreateMap<NewUsuarioDTO, Usuario>();

            //Medico
            CreateMap<Medico, MedicoDTO>();
            CreateMap<MedicoDTO, Medico>();
            CreateMap<Medico, NewMedicoDTO>();
            CreateMap<NewMedicoDTO, Medico>();
            CreateMap<Object, MedicoDTO>();

            //CreateMap<(Medico, Cita, Paciente), MedicoByIdDTO>()
            

            //Paciente
            CreateMap<Paciente, PacienteDTO>();
            CreateMap<PacienteDTO, Paciente>();
            CreateMap<Paciente, NewPacienteDTO>();
            CreateMap<NewPacienteDTO, Paciente>();
            CreateMap<Object, PacienteDTO>();

            //Cita
            CreateMap<Cita, CitaDTO>();
            CreateMap<CitaDTO, Cita>();
            CreateMap<Cita, NewCitaDTO>();
            CreateMap<NewCitaDTO, Cita>();
            CreateMap<Object, CitaDTO>();


            //Diagnostico
            CreateMap<Diagnostico, DiagnosticoDTO>();
            CreateMap<DiagnosticoDTO, Diagnostico>();
            //CreateMap<Diagnostico, NewDiagnosticoDTO>();
            //CreateMap<NewDiagnosticoDTO, Diagnostico>();
        }
    }
}
