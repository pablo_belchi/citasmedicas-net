﻿using citas_medicas.Models;

namespace citas_medicas.Repository
{

    //interfaz generica
    public interface Irepository<T>
    {
        //Operaciones CRUD
        Task<List<T>> GetAll();
        Task<T> GetById(long Id);
        Task Insert(T Obj);
        Task Delete(long Id);
        Task<T> Update(T Obj);
        Object GetMedicoById(long Id);
    }


}
