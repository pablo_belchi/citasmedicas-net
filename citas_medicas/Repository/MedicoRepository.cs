﻿using citas_medicas.Models;
using citas_medicas.Data;

namespace citas_medicas.Repository
{
    public class MedicoRepository : GeneralRepository<Medico, DatabaseContext>
    {

        
        public MedicoRepository(DatabaseContext Context) : base(Context)
        {

        }
    }
}
