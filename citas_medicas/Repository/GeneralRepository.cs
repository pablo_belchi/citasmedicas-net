﻿using citas_medicas.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;

namespace citas_medicas.Repository

{
    public class GeneralRepository<T, Context> : Irepository<T>
        where T : class, ID
        where Context : DbContext
    {

        public readonly Context context;

        public GeneralRepository(Context Context)
        {
            context = Context;
        }


        //FUNCIONES CRUD
        public async Task<List<T>> GetAll()
        {
            List<T> values = await (from elementos in context.Set<T>() select elementos).ToListAsync();
            return values;
        }

        public async Task<T> GetById(long Id)
        {   
            var Query = from elemento in context.Set<T>() where elemento.Id == Id select elemento;
            var resul = Query.FirstOrDefault();

            return resul;
        }

        //Mostrar Medico por Id junto con las citas
        public Object GetMedicoById(long Id)
        {
            List<Object> result = new List<object>(2);

            var Query = from medico in context.Set<Medico>() where medico.Id == Id select medico;

            var InnerJoin = from medico in context.Set<Medico>() join cita in context.Set<Cita>() 
                            on medico.Id equals cita.MedicoId join paciente in context.Set<Paciente>()
                            on cita.PacienteId equals paciente.Id where medico.Id==Id select new {Id_Cita = cita.Id, Fecha_cita = cita.fechaHora, 
                            Motivo_cita = cita.motivoCita, Attribute11= cita.attribute11, Id_Paciente_cita = cita.PacienteId,
                            Nombre_Paciente = paciente.nombre, Apellidos_Paciente = paciente.apellidos, Telefono_Paciente =
                            paciente.telefono, Numero_SS = paciente.nss, Num_tarjeta = paciente.numTarjeta};
            
            var _medico = Query.ToList();
            var cita_Paciente = InnerJoin.ToList();

            result.Add(_medico);
            result.Add(cita_Paciente);

            return result;
        }

        public async Task Insert(T Obj)
        {
            if(Obj.GetType() != typeof(Cita))
            {
                await context.Set<T>().AddAsync(Obj);
                await context.SaveChangesAsync();
            }
            else //Insert de citas junto con diagnostico y relaciones en tabla MedicoPaciente.
            {
                SqlConnection connection;
                SqlCommand command;
                SqlCommand Add_MedicoPaciente;
                SqlCommand count;

                await context.Set<T>().AddAsync(Obj); //Insertamos Cita 
                await context.SaveChangesAsync();

                var Query = (from elemento in context.Set<Cita>() orderby elemento.Id descending select elemento.Id).Take(1);
                var Cita = (from elemento in context.Set<Cita>() orderby elemento.Id descending select elemento).Take(1);
                
                long resul = Query.FirstOrDefault();
                var _cita = Cita.FirstOrDefault();

                string Insert = $"INSERT INTO Diagnostico (Id, valoracionEspecialista, enfermedad) VALUES({resul},'empty','empty');";
                string Insert_MedicoPaciente = $"INSERT INTO MedicoPaciente (MedicoId, PacienteId) VALUES({_cita.MedicoId},{_cita.PacienteId});";
                string Count = $"SELECT COUNT(*) FROM MedicoPaciente WHERE MedicoId={_cita.MedicoId} AND PacienteId={_cita.PacienteId};";
                using (connection = new SqlConnection("Data Source=DESKTOP-HM20IHF; Initial Catalog=Citas_medicas; Integrated Security=True"))
                {
                    command = new SqlCommand(Insert, connection);
                    Add_MedicoPaciente = new SqlCommand(Insert_MedicoPaciente, connection);
                    count = new SqlCommand(Count, connection);

                    await connection.OpenAsync();
                    await Task.Run(() => { command.ExecuteNonQuery(); }); //Insertamos diagnostico con el Id incremental de cita

                    int GetValidate() //Obtenemos coincidencias en MedicoPaciente
                    {
                        var reader = count.ExecuteReader();

                        while (reader.Read())
                        {
                            int match = int.Parse(reader.GetValue(0).ToString());

                            reader.Close();
                            reader.Dispose();
                            return match;
                        }

                        return 0;
                    }

                    if(GetValidate() < 1)
                    {
                        Add_MedicoPaciente.ExecuteNonQuery(); //Insertamos relacion Medico-Paciente
                    }
                    
                    await connection.CloseAsync();

                    await connection.DisposeAsync();
                }

                await context.SaveChangesAsync();
            }
            
        }

        public async Task Delete(long Id)
        {
            var Query = from elemento in context.Set<T>() where elemento.Id == Id select elemento;
            var resul = Query.FirstOrDefault();

            if (resul.GetType() == typeof(Diagnostico))
            {
                await Task.Run( async() =>
                {
                    string Delete = $"DELETE FROM Cita WHERE Id = {Id}";

                    SqlConnection connection;
                    connection = new SqlConnection("Data Source=DESKTOP-HM20IHF; Initial Catalog=Citas_medicas; Integrated Security=True");
                    
                    SqlCommand command;
                    using (connection )
                    {
                        command = new SqlCommand(Delete, connection);
                        await connection.OpenAsync();
                        command.ExecuteNonQuery();

                        await connection.CloseAsync();
                        await connection.DisposeAsync();
                    }
                    
                });

            }
            else {
                await Task.Run(() => { context.Set<T>().Remove(resul); });
            }
            
            await context.SaveChangesAsync();
        }

        public async Task<T> Update(T Obj)
        {
            var Query = from elemento in context.Set<T>() where elemento.Id == Obj.Id select elemento;
            var resul = Query.FirstOrDefault();
            await Task.Run(() => { context.Entry(resul).CurrentValues.SetValues(Obj); });
            await context.SaveChangesAsync();
        
            return Obj;
        }

    }

}