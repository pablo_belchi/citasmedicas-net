﻿using citas_medicas.Models;
using citas_medicas.Data;

namespace citas_medicas.Repository
{
    public class DiagnosticoRepository : GeneralRepository<Diagnostico, DatabaseContext>
    {

        public DiagnosticoRepository(DatabaseContext Context) : base(Context)
        {

        }
    }
}
