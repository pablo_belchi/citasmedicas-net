﻿using citas_medicas.Models;
using citas_medicas.Data;

namespace citas_medicas.Repository
{
    public class CitaRepository : GeneralRepository<Cita, DatabaseContext>
    {
 
        public CitaRepository(DatabaseContext Context) : base(Context)
        {

        }
    }
}
