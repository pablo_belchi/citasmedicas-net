﻿using citas_medicas.Models;
using citas_medicas.Data;

namespace citas_medicas.Repository
{
    public class PacienteRepository : GeneralRepository<Paciente, DatabaseContext>
    {
 
        public PacienteRepository(DatabaseContext Context) : base(Context)
        {

        }
        
    }
}
