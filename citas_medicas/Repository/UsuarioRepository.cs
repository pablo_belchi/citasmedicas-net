﻿using citas_medicas.Models;
using citas_medicas.Data;

namespace citas_medicas.Repository
{
    public class UsuarioRepository : GeneralRepository<Usuario, DatabaseContext>
    {
       

        public UsuarioRepository(DatabaseContext Context) : base(Context)
        {
      
        }
    }
}
