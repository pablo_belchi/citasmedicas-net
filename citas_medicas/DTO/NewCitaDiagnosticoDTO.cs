﻿using Microsoft.AspNetCore.Mvc;

namespace citas_medicas.DTO
{
    public class NewCitaDiagnosticoDTO
    {
        [FromQuery]
        public NewDiagnosticoDTO NewDiagnosticoDTO { get; set; }
        [FromBody]
        public NewCitaDTO NewCitaDTO { get; set; }
    }
}
