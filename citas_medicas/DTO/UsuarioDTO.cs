﻿namespace citas_medicas.DTO
{
    public class UsuarioDTO
    {
        public long Id { get; set; }
        public string nombre { get; set; }
        public string apellidos {  get; set; }
        public string nombreUsuario { get; set; }
    }
}
