﻿namespace citas_medicas.DTO
{
    public class NewPacienteDTO
    {
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string nombreUsuario { get; set; }
        public string clave { get; set; }
        public string nss { get; set; }
        public string numTarjeta { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }


    }
}
