﻿namespace citas_medicas.DTO
{
    public class DiagnosticoDTO
    {
        public long Id { get; set; }
        public string valoracionEspecialista { get; set; }
        public string enfermedad { get; set; }
    }
}
