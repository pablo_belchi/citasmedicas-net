﻿namespace citas_medicas.DTO
{
    public class NewCitaDTO
    {
        public DateTime fechaHora { get; set; }
        public string motivoCita { get; set; }
        public int attribute11 { get; set; }
        public long MedicoId { get; set; }
        public long PacienteId { get; set; }
    }
}
