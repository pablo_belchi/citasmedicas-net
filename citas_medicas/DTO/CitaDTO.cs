﻿namespace citas_medicas.DTO
{
    public class CitaDTO
    {
        public long Id { get; set; }
        public DateTime fechaHora { get; set; }
        public string motivoCita { get; set; }
        public int attribute11 { get; set; }
        public long PacienteId { get; set; }
        public long MedicoId { get; set; }
    }
}