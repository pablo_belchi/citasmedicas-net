﻿namespace citas_medicas.DTO
{
    public class PacienteDTO : UsuarioDTO
    {
        public string nss { get; set; }
        public string numTarjeta { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }

    }
}
