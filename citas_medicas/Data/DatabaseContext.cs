﻿using citas_medicas.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;


namespace citas_medicas.Data
{
    public class DatabaseContext : DbContext 
    {                                       
        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
    
        }

        //Especificacion de entidades
        public DbSet<Diagnostico> Diagnostico { get; set; }
        public DbSet<Medico> Medico { get; set; }
        public DbSet<Paciente> Paciente { get; set; }
        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Cita> Cita { get; set; }
        
        //Sobreescribimos el metodo OnModelCreating para configurar las entidades especificadas anteriormente
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuario>().ToTable("Usuario");
            modelBuilder.Entity<Paciente>().ToTable("Paciente");
            modelBuilder.Entity<Medico>().ToTable("Medico");
            modelBuilder.Entity<Diagnostico>()
                .ToTable("Diagnostico");

            modelBuilder.Entity<Cita>()
                .ToTable("Cita")
                .HasOne( c => c.Diagnostico)
                .WithOne( d => d.Cita)
                .HasForeignKey<Diagnostico>( d => d.Id);
        }
       
    }
}
