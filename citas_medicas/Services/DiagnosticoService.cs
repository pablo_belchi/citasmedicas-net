﻿using citas_medicas.Models;
using citas_medicas.Repository;
using citas_medicas.Services.IServices;
using AutoMapper;

namespace citas_medicas.Services
{
    public class DiagnosticoService : IDiagnosticoService
    {
        Irepository<Diagnostico> repository;
        public DiagnosticoService(Irepository<Diagnostico> repository)
        {
            this.repository = repository;
        }

        //Llamadas a funciones CRUD
        public async Task Delete(long Id) => await repository.Delete(Id);
        public IEnumerable<Diagnostico> GetAll() => repository.GetAll().Result;
        public Diagnostico GetById(long Id) => repository.GetById(Id).Result;
        public async Task Insert(Diagnostico Obj) => await repository.Insert(Obj);
        public async Task<Diagnostico> Update(Diagnostico Obj) => await repository.Update(Obj);
    }
}
