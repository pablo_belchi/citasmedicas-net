﻿using citas_medicas.Models;

namespace citas_medicas.Services.IServices
{
    public interface IMedicoService
    {
        IEnumerable<Medico> GetAll();
        Medico GetById(long Id);
        Object GetMedicoById(long id);
        Task Insert(Medico Obj);
        Task Delete(long Id);
        Task<Medico> Update(Medico Obj);
    }
}
