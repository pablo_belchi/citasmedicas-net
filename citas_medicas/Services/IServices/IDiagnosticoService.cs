﻿using citas_medicas.Models;

namespace citas_medicas.Services.IServices
{
    public interface IDiagnosticoService
    {
        IEnumerable<Diagnostico> GetAll();
        Diagnostico GetById(long Id);
        Task Insert(Diagnostico Obj);
        Task Delete(long Id);
        Task<Diagnostico> Update(Diagnostico Obj);
    }
}
