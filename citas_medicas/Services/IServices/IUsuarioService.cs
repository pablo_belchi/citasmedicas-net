﻿using citas_medicas.Models;

namespace citas_medicas.Services.IServices
{
    public interface IUsuarioService
    {
        IEnumerable<Usuario> GetAll();
        Usuario GetById(long Id);
        Task Insert(Usuario Obj);
        Task Delete(long Id);
        Task<Usuario> Update(Usuario Obj);
    }
}
