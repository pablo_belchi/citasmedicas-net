﻿using citas_medicas.Models;
namespace citas_medicas.Services.IServices
{
    public interface ICitaService
    {
        IEnumerable<Cita> GetAll();
        Cita GetById(long Id);
        Task Insert(Cita Obj);
        Task Delete(long Id);
        Task<Cita> Update(Cita Obj);
    }
}
