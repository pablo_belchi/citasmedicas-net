﻿using citas_medicas.Models;

namespace citas_medicas.Services.IServices
{
    public interface IPacienteService
    {
        IEnumerable<Paciente> GetAll();
        Paciente GetById(long Id);
        Task Insert(Paciente Obj);
        Task Delete(long Id);
        Task<Paciente> Update(Paciente Obj);
    }
}
