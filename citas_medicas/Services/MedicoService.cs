﻿using citas_medicas.Models;
using citas_medicas.Repository;
using citas_medicas.Services.IServices;

namespace citas_medicas.Services
{
    public class MedicoService : IMedicoService
    {
        Irepository<Medico> repository;
        public MedicoService(Irepository<Medico> repository)
        {
            this.repository = repository;
        }

        //Llamadas a funciones CRUD
        public async Task Delete(long Id) => await repository.Delete(Id);
        public IEnumerable<Medico> GetAll() => repository.GetAll().Result;
        public Medico GetById(long Id) => repository.GetById(Id).Result;
        public Object GetMedicoById(long Id) => repository.GetMedicoById(Id);
        public async Task Insert(Medico Obj) => await repository.Insert(Obj);
        public async Task<Medico> Update(Medico Obj) => await repository.Update(Obj);
    }
}
