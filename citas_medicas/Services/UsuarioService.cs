﻿using AutoMapper;
using citas_medicas.Models;
using citas_medicas.Repository;
using citas_medicas.Services.IServices;

namespace citas_medicas.Services
{
    public class UsuarioService : IUsuarioService
    {
        Irepository<Usuario> repository;
        public UsuarioService(Irepository<Usuario> repository)
        {
            this.repository = repository;
        }

        //Llamadas a funciones CRUD
        public async Task Delete(long Id) => await repository.Delete(Id);
        public IEnumerable<Usuario> GetAll() => repository.GetAll().Result;
        public Usuario GetById(long Id) => repository.GetById(Id).Result;
        public async Task Insert(Usuario Obj) => await repository.Insert(Obj);
        public async Task<Usuario> Update(Usuario Obj) => await repository.Update(Obj);
    }
}
