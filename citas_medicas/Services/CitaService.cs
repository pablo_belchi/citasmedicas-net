﻿using citas_medicas.Models;
using citas_medicas.Repository;
using citas_medicas.Services.IServices;

namespace citas_medicas.Services.IServices
{
    public class CitaService : ICitaService
    {
        public Irepository<Cita> repository;
        public Irepository<Diagnostico> diagnosticoRepository;
        private DateTime FechaHoraActual = DateTime.Today;
        
        public CitaService(Irepository<Cita> repository, Irepository<Diagnostico> diagnosticoRepository)
        {
            this.repository = repository;
            this.diagnosticoRepository = diagnosticoRepository;
        }

        //Llamadas a funciones CRUD
        public IEnumerable<Cita> GetAll() => repository.GetAll().Result;
        public Cita GetById(long Id) => repository.GetById(Id).Result;
        public async Task Insert(Cita Obj) => await repository.Insert(Obj);
        public async Task Delete(long Id) => await repository.Delete(Id);
        public async Task<Cita> Update(Cita Obj) => await repository.Update(Obj);
    }
}
