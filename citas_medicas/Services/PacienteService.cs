﻿using citas_medicas.Models;
using citas_medicas.Repository;
using citas_medicas.Services.IServices;

namespace citas_medicas.Services
{
    public class PacienteService : IPacienteService
    {
        Irepository<Paciente> repository;
        public PacienteService(Irepository<Paciente> repository)
        {
            this.repository = repository;
        }

        //Llamadas a funciones CRUD
        public async Task Delete(long Id) => await repository.Delete(Id);
        public IEnumerable<Paciente> GetAll() => repository.GetAll().Result;
        public Paciente GetById(long Id) => repository.GetById(Id).Result;
        public async Task Insert(Paciente Obj) => await repository.Insert(Obj);
        public async Task<Paciente> Update(Paciente Obj) => await repository.Update(Obj);
    }
}
