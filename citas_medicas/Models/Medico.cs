﻿using System.ComponentModel.DataAnnotations;

namespace citas_medicas.Models
{
    public class Medico : Usuario
    {
        [Required]
        public string numColegiado { get; set; }

        public Medico(string numColegiado, string nombre, string apellidos, string nombreUsuario, string clave) : base(nombre, apellidos, nombreUsuario, clave)
        {
            this.numColegiado = numColegiado;
        }
 
        //prop de navegación
        public IList<Paciente> Paciente { get; set; }
        public IList<Cita> Cita { get; set; }  
    }
}
