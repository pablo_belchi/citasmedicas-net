﻿using System.ComponentModel.DataAnnotations;

namespace citas_medicas.Models
{
    public class Usuario : ID
    {
        [Key]
        public long Id { get; set; }
        [StringLength(25)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        public string nombre { get; set; }
        [StringLength(50)]
        [RegularExpression(@"^[A-Z]+[a-zA-Z]*$")]
        public string apellidos {  get; set; }
        [StringLength(50)]
        public string nombreUsuario { get; set; }
        public string clave { get; set; }


        public Usuario(string nombre, string apellidos, string nombreUsuario, string clave)
        {
            this.nombre = nombre;
            this.apellidos = apellidos;
            this.nombreUsuario = nombreUsuario;
            this.clave = clave;

        }
    }
}
