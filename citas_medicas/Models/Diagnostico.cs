﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace citas_medicas.Models
{
    public class Diagnostico : ID
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [ForeignKey("Id")]
        public string valoracionEspecialista { get; set; }
        public string enfermedad { get; set; }

        public Diagnostico(string valoracionEspecialista, string enfermedad)
        {
            this.valoracionEspecialista = valoracionEspecialista;
            this.enfermedad = enfermedad;
        }

        //prop de navegación
        public virtual Cita Cita { get; set; }
     
    }
}
