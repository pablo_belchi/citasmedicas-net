﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace citas_medicas.Models
{
    public class Cita:ID
    {

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public long Id { get; set; }
        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime fechaHora { get; set; }
        public string motivoCita { get; set; }
        public int attribute11 { get; set; }


        public Cita(string motivoCita, int attribute11, DateTime fechaHora)
        {
            this.fechaHora = fechaHora;
            this.motivoCita = motivoCita;
            this.attribute11 = attribute11;

        }

        //prop de navegación e Ids
        public virtual Diagnostico Diagnostico { get; set; }
        [ForeignKey("PacienteId")]
        public virtual long PacienteId { get; set; }
        public virtual Paciente Paciente { get; set; }
        [ForeignKey("MedicoId")]
        public virtual long MedicoId { get; set; }
        public virtual Medico Medico { get; set; }
    }
}
