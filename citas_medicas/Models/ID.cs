﻿namespace citas_medicas.Models
{
    public interface ID
    {
        long Id { get; set; }
    }
}
