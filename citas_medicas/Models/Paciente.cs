﻿namespace citas_medicas.Models
{
    public class Paciente : Usuario
    {
        public string nss { get; set; }
        public string numTarjeta { get; set; }
        public string telefono { get; set; }
        public string direccion { get; set; }

        public Paciente(string nombre, string apellidos, string nombreUsuario, string clave, string nss, string numTarjeta, string telefono, string direccion) : base(nombre, apellidos, nombreUsuario, clave)
        {
            this.nss = nss;
            this.numTarjeta = numTarjeta;
            this.telefono = telefono;
            this.direccion = direccion;

        }

        //prop de navegación
        public IList<Medico> Medico { get; set; }
        public IList<Cita> Cita { get; set; }
    }
}
