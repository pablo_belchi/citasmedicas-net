﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using citas_medicas.Data;

#nullable disable

namespace citas_medicas.Migrations
{
    [DbContext(typeof(DatabaseContext))]
    [Migration("20220406172110_segundamigracion")]
    partial class segundamigracion
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "6.0.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder, 1L, 1);

            modelBuilder.Entity("citas_medicas.Models.Cita", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<long>("Id"), 1L, 1);

                    b.Property<long>("DiagnosticoId")
                        .HasColumnType("bigint");

                    b.Property<long>("MedicoId")
                        .HasColumnType("bigint");

                    b.Property<long>("PacienteId")
                        .HasColumnType("bigint");

                    b.Property<int>("attribute11")
                        .HasColumnType("int");

                    b.Property<DateTime>("fechaHora")
                        .HasColumnType("datetime2");

                    b.Property<string>("motivoCita")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.HasIndex("DiagnosticoId")
                        .IsUnique();

                    b.HasIndex("MedicoId");

                    b.HasIndex("PacienteId");

                    b.ToTable("Cita", (string)null);
                });

            modelBuilder.Entity("citas_medicas.Models.Diagnostico", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<long>("Id"), 1L, 1);

                    b.Property<string>("enfermedad")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("valoracionEspecialista")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Diagnostico", (string)null);
                });

            modelBuilder.Entity("citas_medicas.Models.Usuario", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("bigint");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<long>("Id"), 1L, 1);

                    b.Property<string>("apellidos")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.Property<string>("clave")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("nombre")
                        .IsRequired()
                        .HasMaxLength(25)
                        .HasColumnType("nvarchar(25)");

                    b.Property<string>("nombreUsuario")
                        .IsRequired()
                        .HasMaxLength(50)
                        .HasColumnType("nvarchar(50)");

                    b.HasKey("Id");

                    b.ToTable("Usuario", (string)null);
                });

            modelBuilder.Entity("MedicoPaciente", b =>
                {
                    b.Property<long>("MedicoId")
                        .HasColumnType("bigint");

                    b.Property<long>("PacienteId")
                        .HasColumnType("bigint");

                    b.HasKey("MedicoId", "PacienteId");

                    b.HasIndex("PacienteId");

                    b.ToTable("MedicoPaciente");
                });

            modelBuilder.Entity("citas_medicas.Models.Medico", b =>
                {
                    b.HasBaseType("citas_medicas.Models.Usuario");

                    b.Property<string>("numColegiado")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.ToTable("Medico", (string)null);
                });

            modelBuilder.Entity("citas_medicas.Models.Paciente", b =>
                {
                    b.HasBaseType("citas_medicas.Models.Usuario");

                    b.Property<string>("direccion")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("nss")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("numTarjeta")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("telefono")
                        .IsRequired()
                        .HasColumnType("nvarchar(max)");

                    b.ToTable("Paciente", (string)null);
                });

            modelBuilder.Entity("citas_medicas.Models.Cita", b =>
                {
                    b.HasOne("citas_medicas.Models.Diagnostico", "Diagnostico")
                        .WithOne("Cita")
                        .HasForeignKey("citas_medicas.Models.Cita", "DiagnosticoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("citas_medicas.Models.Medico", "Medico")
                        .WithMany("Cita")
                        .HasForeignKey("MedicoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("citas_medicas.Models.Paciente", "Paciente")
                        .WithMany("Cita")
                        .HasForeignKey("PacienteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Diagnostico");

                    b.Navigation("Medico");

                    b.Navigation("Paciente");
                });

            modelBuilder.Entity("MedicoPaciente", b =>
                {
                    b.HasOne("citas_medicas.Models.Medico", null)
                        .WithMany()
                        .HasForeignKey("MedicoId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("citas_medicas.Models.Paciente", null)
                        .WithMany()
                        .HasForeignKey("PacienteId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("citas_medicas.Models.Medico", b =>
                {
                    b.HasOne("citas_medicas.Models.Usuario", null)
                        .WithOne()
                        .HasForeignKey("citas_medicas.Models.Medico", "Id")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();
                });

            modelBuilder.Entity("citas_medicas.Models.Paciente", b =>
                {
                    b.HasOne("citas_medicas.Models.Usuario", null)
                        .WithOne()
                        .HasForeignKey("citas_medicas.Models.Paciente", "Id")
                        .OnDelete(DeleteBehavior.ClientCascade)
                        .IsRequired();
                });

            modelBuilder.Entity("citas_medicas.Models.Diagnostico", b =>
                {
                    b.Navigation("Cita")
                        .IsRequired();
                });

            modelBuilder.Entity("citas_medicas.Models.Medico", b =>
                {
                    b.Navigation("Cita");
                });

            modelBuilder.Entity("citas_medicas.Models.Paciente", b =>
                {
                    b.Navigation("Cita");
                });
#pragma warning restore 612, 618
        }
    }
}
