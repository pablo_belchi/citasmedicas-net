using citas_medicas.Data;
using citas_medicas.Mapping;
using citas_medicas.Models;
using citas_medicas.Repository;
using citas_medicas.Services;
using citas_medicas.Services.IServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<DatabaseContext>(
        options => options.UseSqlServer("Data Source=DESKTOP-HM20IHF; Initial Catalog=Citas_medicas; Integrated Security=True"));

builder.Services.AddScoped<Irepository<Cita>,CitaRepository>();
builder.Services.AddScoped<Irepository<Diagnostico>, DiagnosticoRepository >();
builder.Services.AddScoped<Irepository<Paciente>, PacienteRepository>();
builder.Services.AddScoped<Irepository<Medico>, MedicoRepository >();
builder.Services.AddScoped<Irepository<Usuario>, UsuarioRepository >();

builder.Services.AddScoped<ICitaService, CitaService>();
builder.Services.AddScoped<IDiagnosticoService, DiagnosticoService>();
builder.Services.AddScoped<IMedicoService, MedicoService>();
builder.Services.AddScoped<IPacienteService, PacienteService>();
builder.Services.AddScoped<IUsuarioService, UsuarioService>();

builder.Services.AddControllersWithViews();

builder.Services.AddAutoMapper(typeof(AutoMapperProfile));

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "Citas Medicas Documentación",
        Version = "v1",
        Description = "REST API para Citas_médicas"
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.XML";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    options.IncludeXmlComments(xmlPath);
});

var app = builder.Build();
// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
else
{
    app.UseSwaggerUI(options =>
    {
        options.SwaggerEndpoint("swagger/v1/swagger.json", "citas_medicas v1");
        options.RoutePrefix = string.Empty;
    });

    app.UseSwaggerUI();
}

app.UseSwagger();

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.Run();
